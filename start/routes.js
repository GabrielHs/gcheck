"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.get("/", () => {
  return { 
    Welcome: "API gcheck",
    info: "1.0.0" 
   };
});

Route.post("/user/add", "UserController.create");

Route.post("/user/auth", "UserController.login");

Route.post("/user/permissao", "UserController.getPapel").middleware(["auth"]);

Route.post("/user/logout", "UserController.logoutUser").middleware(["auth"]);

Route.get("/admin/user/get/:documento", "UserController.getUser").middleware([
  "auth",
]);

Route.post("/admin/add", "CheckController.addCheck").middleware(["auth"]);

Route.get("/user/checks", "CheckController.getChecksUser").middleware([
  "auth",
]);
