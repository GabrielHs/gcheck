"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Papel extends Model {
  static get hidden() {
    return ["id", "user_id", "updated_at", "created_at"];
  }
}

module.exports = Papel;
