"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Check extends Model {
  static get hidden() {
    return ["id", "user_id", "updated_at"];
  }
}

module.exports = Check;
