"use strict";

const Check = use("App/Models/Check");
const User = use("App/Models/User");

class CheckController {
  async addCheck({ request, view, response, auth }) {
    try {
      let isLogado = await auth.check();

      if (!isLogado) {
        response.status(401).send("Você não está logado");
      }

      //pelo token do admin e captura o username
      let userAdminLogado = auth.user.username;

      let userLogado = await User.findBy("username", userAdminLogado);

      //Carrega o papel daquele usuario
      await userLogado.load("papeis");

      //convert o model dados para um objeto json e poder manipula-lo
      let adminJson = userLogado.toJSON();

      //Somente admin(token admin) pode realizar essas buscas
      if (adminJson.papeis.type !== "admin") {
        return response.status(401).send({
          error: "você não tem permissaão para realizar esse processo",
        });
      }

      let { documento } = request.all();

      if (documento) {
        let user = await User.findBy("documento", documento);

        //Salva no bd os dados o papel de user
        await Check.create({ user_id: user.id, contagem: 1 });

        let quantidadeChecks = await Check.getCount("contagem");

        return response
          .status(201)
          .send({ ok: `cadastrado com sucesso`, qtnChecks: quantidadeChecks });
      } else {
        return response
          .status(400)
          .send({ error: `documento não foi passado` });
      }
    } catch (error) {
      return response.status(500).send({ error: `Erro: ${error.message}` });
    }
  }

  async getChecksUser({ request, view, response, auth }) {
    try {
      let isLogado = await auth.check();

      if (!isLogado) {
        response.status(401).send("Você não está logado");
      }
      let userToken = auth.user;

      let userLogado = await User.findBy("id", userToken.id);

      await userLogado.load("checks");

      let userObj = userLogado.toJSON();

      return userObj;
    } catch (error) {
      return response.status(500).send({ error: `Erro: ${error.message}` });
    }
  }
}

module.exports = CheckController;
