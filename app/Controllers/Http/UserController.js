"use strict";

const User = use("App/Models/User");
const Papel = use("App/Models/Papel");
const Hash = use("Hash");
const { validateAll } = use("Validator");

class UserController {
  /**
   * Show the form for creating a new resource.
   */
  async create({ request, response }) {
    try {
      let errorMsgCustom = {
        "username.required": "Esse campo é obrigátorio",
        "username.unique": "Esse username já existe",
        "username.min": "Esse campo de conter no minimo 5 caracteres",
        "password.required": "Esse campo é obrigátorio",
        "password.min": "Esse campo de conter no minimo 6 caracteres",
        "documento.required": "Esse campo é obrigátorio",
        "documento.min": "Esse documento de conter no minimo 12 caracteres",
        "documento.unique": "Essa documento já existe",
        "veiculo_name.required": "Esse campo é obrigátorio",
        "veiculo_name.min": "Esse campo de conter no minimo 4 caracteres",
        "placa.required": "Esse campo é obrigátorio",
        "placa.min": "Esse campo de conter no minimo 4 caracteres",
        "placa.unique": "Essa placa já existe",
        "name.required": "Esse campo é obrigátorio",
        "placa.min": "Esse campo de conter no minimo 3 caracteres",
      };

      //Metodo de validação
      let validation = await validateAll(
        request.all(),
        {
          username: "required|min:5,|unique:users",
          password: "required|min:6",
          documento: "required|min:10|unique:users",
          veiculo_name: "required|min:2",
          placa: "required|min:4|unique:users",
          name: "required|min:3",
        },
        errorMsgCustom
      );

      //Caso tenha erro, retornará as msg
      if (validation.fails()) {
        return response.status(401).send({
          error: validation.messages(),
        });
      }

      let data = request.only([
        "username",
        "password",
        "documento",
        "veiculo_name",
        "placa",
        "name",
      ]);

      //Salva no bd os dados de user
      let user = await User.create(data);

      //Salva no bd os dados o papel de user
      await Papel.create({ user_id: user.id });

      return response.status(201).send({
        ok: {
          username: user.username,
          nameCliente: user.name,
          msg: "Cadastrado com sucesso",
        },
      });
    } catch (error) {
      return response.status(500).send({
        error: "Erro ao salvar dados " + error.message,
      });
    }
  }

  //ACTION DE AUTENTICAÇÃO
  async login({ request, view, response, auth }) {
    try {
      let errorMsgCustom = {
        "username.required": "O campo usúario é obrigátorio",
        "password.required": "O campo Senha é obrigátorio",
      };

      let validation = await validateAll(
        request.all(),
        {
          username: "required",
          password: "required",
        },
        errorMsgCustom
      );

      if (validation.fails()) {
        return response
          .status(200)
          .send({ message: { error: validation.messages() } });
      }
      let { username, password } = request.all();

      let hasUser = await User.findBy("username", username);

      if (!hasUser) {
        return response
          .status(200)
          .send({ message: { error: `Usúario não encontrado` } });
      }

      let passwordCheck = await Hash.verify(password, hasUser.password);

      if (!passwordCheck) {
        return response
          .status(200)
          .send({ message: { error: "Senha incorreta" } });
      }

      //função do adonis que valida o usuario e retorna um token
      let validaToken = auth.attempt(username, password);

      return validaToken;
    } catch (error) {
      return response.status(500).send({ error: `Erro: ${error.message}` });
    }
  }

  async getPapel({ request, view, response, auth }) {
    let { username } = request.post();
    let user = await User.findBy("username", username);

    await user.load("papeis");

    return user;
  }

  async logoutUser({ request, view, response, auth }) {
    let { token } = request.post();
    if (!token) {
      // You can throw any exception you want here
      throw BadRequestException.invoke(`Atualizar token ausente`);
    }

    await auth.authenticator("jwt").revokeTokens([token], true);

    return response.send({ status: 200, "efetuado logout": "success" });
  }

  async getUser({ params, request, view, response, auth }) {
    try {
      //pelo token do admin e captura o username
      let userAdminLogado = auth.user.username;

      let userLogado = await User.findBy("username", userAdminLogado);

      //Carrega o papel daquele usuario
      await userLogado.load("papeis");

      //convert o model dados para um objeto json e poder manipula-lo
      let adminJson = userLogado.toJSON();

      //Somente admin(token admin) pode realizar essas buscas
      if (adminJson.papeis.type !== "admin") {
        return response.status(401).send({
          error: "você não tem permissaão para realizar esse processo",
        });
      }

      let documento = params.documento;

      let user = await User.findBy("documento", documento);

      if (!user) {
        return response.status(201).send({ error: `usuário não encontrado ` });
      }

      await user.load("papeis");
      await user.load("checks");

      let userJson = user.toJSON();

      return userJson;
    } catch (error) {
      return response.status(500).send({ error: `Erro: ${error.message}` });
    }
  }
}

module.exports = UserController;
