'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CheckSchema extends Schema {
  up () {
    this.create('checks', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users').onUpdate('CASCADE').onDelete('CASCADE')
      table.integer('contagem').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('checks')
  }
}

module.exports = CheckSchema
