'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('name', 60).notNullable()
      table.string('password', 60).notNullable()
      table.string('documento', 20).notNullable().unique()
      table.string('veiculo_name', 30).notNullable()
      table.string('placa', 20).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
