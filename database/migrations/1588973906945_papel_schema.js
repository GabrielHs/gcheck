'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PapelSchema extends Schema {
  up () {
    this.create('papels', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users').onUpdate('CASCADE').onDelete('CASCADE')
      table.string('type', 20).defaultTo("user")
      table.timestamps()
    })
  }

  down () {
    this.drop('papels')
  }
}

module.exports = PapelSchema
